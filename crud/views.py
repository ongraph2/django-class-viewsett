
from django.shortcuts import render
from rest_framework import serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Students
from .serializer import StudentSerializer
from rest_framework import status
# Create your views here.

class getAPI(APIView):
    def get(self,request,pk=None,format=None):
        id = pk
        if id is not None:
            stu = Students.objects.get(id=id)
            serializer = StudentSerializer(stu)
            return Response(serializer.data)
        stu = Students.objects.all()
        serializer = StudentSerializer(stu, many=True)
        return Response(serializer.data)

class CreateAPI(APIView):
    def post(self,request,format=None):
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'Data Created!!'},status=status.HTTP_201_CREATED)
        return Response(serializer.errors)

class PutAPI(APIView):
    def put(self,request,pk,format=None):
        id = pk
        stu = Students.objects.get(id=id)
        serializer = StudentSerializer(stu,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'Complete Data Updated!!'})
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    
class PatchAPI(APIView):
    def patch(self,request,pk,format=None):
        id = pk
        stu = Students.objects.get(id=id)
        serializer = StudentSerializer(stu,data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'Partial Data Updated!!'})
        return Response(serializer.errors)
    
class DeleteAPI(APIView):
    def delete(self,request, pk, format=None):
        id = pk
        stu = Students.objects.get(id=id)
        stu.delete()
        return Response({'msg':'Data Deleted!!'})